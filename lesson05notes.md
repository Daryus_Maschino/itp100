
#"Daryus Maschino: 5A 05 notes"

 
1.How can we use Markdown and git to create nice looking documents under revision control?

I am not entirely sure what is being asked here, but using Markdown and git in revision control allow us to manage and change files.

2.Dr. Chuck describes functions as store and reused pattern in programming. Explain what he means by this. (Note: I will present another view of functions, as a tool for allowing procedural abstraction, in our class discussion, and will emphasize just what an important idea that is).

When reffering to functions as "stored and reused patterns in programming," he is reffering to the fact that a function is essentialy a call to a program or set of programs that can be used over and over again as needed.

3.How do we create functions in Python? What new keyword do we use? Provide your own example of a function written in Python.

We use the "def" command in order to create a new function. In order to create a function I could use: def (example) and then I would have to give it code to execute, but once I would have done that I would be good to go. 

4.Dr. Chuck shows that nothing is output when you define a function, what he calls the store phase. What does he call the process that makes the function run? He uses two words for this, and it is really important to understand this idea and learn the words for it.

The second phase he describes is the reuse phase, where you are calling upon the function and thus getting a definite output, thus, the terms store and reuse are the ones he puts a lot of emphisis on.

5.Provide some examples of built-in function in Python.
So there is: print(), input(), type(), float(), and int().

			      	#Notes on lesson 9, Daryus Maschino

1. In the first video of this lesson, Dr. Chuck discusses two very important concepts: algorithms and data structures. How does he define these two concepts? Which one does he say we have been focusing on until now?

Acording to Dr.Chuck, "Algorithms are a set of rules used to solve a problem," while data structures are "A particular way of organizing data in a computer. Until now, the main system we have been using is the algortimic system.

2. Describe in your own words what a collection is.

With a collection, we can assign multiple values to a variable, effectively allowing us to make lists

3. Dr. Chuck makes a very important point in the slide labeled Lists and definite loops - best pals about Python and variable names that he has made before, but which bares repeating. What is that point?

He mentions the utility for us to have singular and plural names for things like variables, but states that unlike us, the machine is incapable of making that distinction.

4. How do we access individual items in a list?

In order to access a given thing in a list, you have to identify the numerical position of that given thing within the list.

5. What does mutable mean? Provide examples of mutable and immutable data types in Python.

Mutable reffers to being able to change, so not concreate. In terms of mutable data types, there are variables, which can be changed rather easily, contrasting form variables, strings are not mutable.

6. Discribe several list operations presented in the lecture.

Lists can be concatenated, by adding preexisting lists together, they can also be sliced,and you can also use the range of a function which gives you a list running from zero to one place less than the perameter.

7. Discribe several list methods presented in the lecture.

So, you can use count to count the number of items in the list, extend adds things to the end of the list, index searchers the list for a given term, insert adds a term to the middle of the list, pop pulls somthing from the begining of the list, remove removes a term, reverse reverses the order, and sort sorts the contents of the list based off of their value

8. Discribe several useful functions that take lists as arguments presented in the lecture.

Len, gives the length of the list; max, displays the largest; min, displays the minimum value; and sum gives the sum of all the values.

9. The third video describes several methods that allow lists and strings to interoperate in very useful ways. Describe these.

The split function allows for the splitting of a string into peices, strip removes the end of a new line, and  starswith checks the first bit of the line. 

10. What is a guardian pattern? Use at least one specific example in describing this important concept.
A gaurdian pattern is a means of preventing your code from "blowing up," by ensuring that if a conditional is such that it would lead to the code faulting, it would never reach that point in the first place, in Dr.Chuck's example, he has an if statment who's perameter is "line == ''" so that if there is no input given, the code will just continue on inspite of that.


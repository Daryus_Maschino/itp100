# Daryus Maschino lesson 6 notes

1.Dr. Chuck calls looping the 4th basic pattern of computer programming. What are the other three patterns?
The four basic patterns of computer programing as Dr.Chuck phrased is are sequential, conditional, store and reuse, and loops and iteration.

2.Describe the program Chr. Chuck uses to introduce the while loop. What does it do?
Dr.Chuck's innitial program was pretty simple, he established a variable, set said varible to 5 as he was doing that, then in his loop he set the parameters to check if the varaible was greater than 0, if it was, it would subtract 1 from the value of his innitial variable and in print out the current value of it. It would repeat this process until it reached 0, at which point it would print "blast off" and the loop would end

3.What is another word for looping introduced in the video?
Dr.Chuck uses the term "repeated steps" when refering to loops, to illustrate the fact that they are essentially a series of contiuouslyrepeating steps that end only when they are instructed to.

4.Dr. Chuck shows us a broken example of a loop. What is this kind of loop called? What is wrong with it?
He demonstraits a zero trip loop, which is a loop that only executes the command at the end of the loop (the "else" bit) and in doing so ignores the commands that would normally repeat until the end of the function.

5.What Python statement (a new keyword for us, by the way) can be used to exit a loop?
The "break" statment breaks out of the loop.

6.What is the other loop control statement introduced in the first video? What does it do?
The "continue" statement ends the current itteration of the loop, then it starts back at the top of the itteration.

7.Dr. Chuck ends the first video by saying that a while is what type of loop? Why are they called that? What is the type of loop he says will be discussed next?
He states that while loops are "indefinite" loops, in that they go on until a given logical condition becomes false.

8.Which new Python loop is introduced in the second video? How does it work?
In  the second video, Dr.Chuck itroduces the concept of a definite loop, which is a loop that comes to a conclusion after a given number of itterations

9.The third video introduces what Dr. Chuck calls loop idioms. What does he mean by that term? Which examples of loop idioms does he introduce in this and the fourth video?
Loop idioms are patterns in a loop used in order to solve a given problem with a loop. One of the more notable examples he gave, in my mind, was his greatest number loop, in which he created a loop that went through a list of numbers and then determined the largest number amongst them after each loop interval.

